# coder-desafio-33

Heroku

## endpoints:

[GET]/api/productos - Lista todos los productos

[GET]/api/productos/:id - Trae un único producto por id

[POST] /api/productos - Guarda un producto en la base

[PUT] /api/productos/:id - Modifica un producto por id

[DELETE] /api/productos/:id - Elimina un producto por id

[GET] /info - Muestra información del global process

[GET] /random?cant=99999 - calculo un numero random de 1 a 1000 por la cant de veces recibida

[POST] /forceShutdown - apago el server y muestro por consola el exit code

## UI

https://coder-desafio-33.herokuapp.com/ingreso (autenticado unicamente)

https://coder-desafio-33.herokuapp.com/login

https://coder-desafio-33.herokuapp.com/register

## Inicio del server

nodemon

pm1 start src/server.js --name "server1fork" -- FORK 8081

pm2 start src/server.js --name "server2cluster" -- CLUSTER 8082

aplicar en nginx config la configuración de nginx.conf
